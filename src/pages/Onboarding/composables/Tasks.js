import { ref } from 'vue';

import Task from '../scripts/Task';
import Todo from '../scripts/Todo';

const sampleTask = new Task('Todo List', 'January 08, 2024');
sampleTask.inProcessTodos.push(
  new Todo(sampleTask.id, 'Continuation Of To-Do List Design', '8:00AM')
);

const Tasks = ref([sampleTask]);

const addNewTask = (newTask) => {
  Tasks.value.push(newTask);
};

const setTasks = (newTasks) => {
  Tasks.value = newTasks;
};

const getTaskById = (taskId) => {
  return Tasks.value.find((task) => task.id === taskId);
};

export { Tasks, addNewTask, getTaskById, setTasks };
