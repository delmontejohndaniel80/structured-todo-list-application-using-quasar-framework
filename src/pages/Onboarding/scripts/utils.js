// Returns a time with AM/PM appended
const getMeridiem = (time) => {
  const hour = parseInt(time, 10);
  return hour >= 0 && hour < 12 ? 'AM' : 'PM';
};

export { getMeridiem };
