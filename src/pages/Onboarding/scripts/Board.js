import { defineComponent } from 'vue';
import { useRouter } from 'vue-router';
import TodoItem from '../components/TodoItem.vue';

export default defineComponent({
  props: {
    defaultOpened: {
      type: Boolean,
      default: false
    },
    taskId: {
      type: Number,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    date: {
      type: String,
      required: false
    },
    todos: {
      type: Array
    },
    finished: {
      type: Boolean,
      required: false
    }
  },
  components: {
    TodoItem
  },
  emits: ['delete'],
  setup(props, context) {
    const router = useRouter();

    const handleDelete = () => {
      context.emit('delete', props.taskId);
    };

    const handleEdit = () => {
      router.push({ name: 'add-new-task', params: { taskId: props.taskId } });
    };

    return {
      handleDelete,
      handleEdit
    };
  }
});
