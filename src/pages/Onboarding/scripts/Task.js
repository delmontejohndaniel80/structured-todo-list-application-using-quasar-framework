class Task {
  static id = 1;

  constructor(title, date, inProcessTodos = [], doneTodos = []) {
    this._id = Task.id++;
    this._title = title;
    this._date = date;
    this._inProcessTodos = inProcessTodos;
    this._doneTodos = doneTodos;
  }

  get id() {
    return this._id;
  }

  get title() {
    return this._title;
  }

  get date() {
    return this._date;
  }

  get inProcessTodos() {
    return this._inProcessTodos;
  }

  set inProcessTodos(newValue) {
    return (this._inProcessTodos = newValue);
  }

  get doneTodos() {
    return this._doneTodos;
  }

  set doneTodos(newValue) {
    this._doneTodos = newValue;
  }
}

export default Task;
