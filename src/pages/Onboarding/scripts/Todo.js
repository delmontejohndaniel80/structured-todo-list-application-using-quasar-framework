class Todo {
  static id = 1;

  constructor(taskId, name, time) {
    this._id = Todo.id++;
    this._taskId = taskId;
    this._name = name;
    this._time = time;
    this._done = false;
  }

  get id() {
    return this._id;
  }

  get taskId() {
    return this._taskId;
  }

  get name() {
    return this._name;
  }

  get time() {
    return this._time;
  }

  get done() {
    return this._done;
  }

  set done(newValue) {
    this._done = newValue;
  }
}

export default Todo;
