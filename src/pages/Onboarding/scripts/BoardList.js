import { useQuasar } from 'quasar';
import { defineComponent, ref, computed } from 'vue';
import Board from '../components/Board.vue';
import { Tasks, setTasks, getTaskById } from '../composables/Tasks';

import MainDialog from 'src/components/MainDialog.vue';
import { ToggleMainDialogState } from 'src/composables/Triggers';
import CustomModal from '../components/CustomModal.vue';

export default defineComponent({
  name: 'BoardList',
  props: {
    search: {
      type: String,
      required: true
    }
  },
  components: {
    Board,
    MainDialog,
    CustomModal
  },
  setup(props) {
    const $q = useQuasar();

    const filteredTasks = computed(() => {
      return Tasks.value.filter((task) => task.title.includes(props.search));
    });

    const selectedTaskId = ref(null);

    const showDialog = (taskId) => {
      selectedTaskId.value = taskId;
      ToggleMainDialogState();
    };

    const handleAgree = () => {
      const task = getTaskById(selectedTaskId.value);
      const newTasks = Tasks.value.filter((t) => t !== task);
      setTasks(newTasks);
      selectedTaskId.value = null;
      ToggleMainDialogState();

      $q.notify({
        html: true,
        color: 'yellow-2',
        textColor: 'green',
        message:
          '<strong>Successfully Deleted!</strong><br />To-do List has been Deleted successfully!',
        position: 'bottom-right',
        actions: [
          {
            icon: 'close',
            color: 'green',
            round: true,
            size: 'sm'
          }
        ]
      });
    };

    const handleDisagree = () => {
      ToggleMainDialogState();
    };

    return {
      Tasks,
      filteredTasks,
      showDialog,
      handleAgree,
      handleDisagree
    };
  }
});
