import { useQuasar } from 'quasar';
import { defineComponent, ref, onMounted } from 'vue';
import { useRouter } from 'vue-router';
import {
  TodoInputs,
  addNewTodoInputs,
  removeTodoInputsById,
  clearTodoInputs,
  setTodoInputs
} from '../composables/TodoInputs';
import { getTaskById, addNewTask } from '../composables/Tasks';
import { getMeridiem } from './utils';

import Task from './Task';
import Todo from './Todo';

export default defineComponent({
  name: 'AddNewTask',
  props: {
    taskId: {
      type: String,
      required: false
    }
  },
  setup(props) {
    const router = useRouter();
    const taskTitle = ref(null);
    const $q = useQuasar();

    const createNewTask = () => {
      TodoInputs.value.forEach((todo) => console.log(todo.time));

      const getCurrentDate = () => {
        const currentDate = new Date();

        const months = [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December'
        ];

        const month = months[currentDate.getMonth()];
        const day = currentDate.getDate();
        const year = currentDate.getFullYear();

        const formattedDate =
          month + ' ' + (day < 10 ? '0' : '') + day + ', ' + year;

        return formattedDate;
      };

      const newTask = new Task(taskTitle.value, getCurrentDate());
      const todos = TodoInputs.value.map((todo) => {
        const time = `${todo.time} ${getMeridiem(todo.time)}`;
        return new Todo(newTask.id, todo.taskName, time);
      });

      newTask.inProcessTodos = todos;

      addNewTask(newTask);
      clearTodoInputs();

      $q.notify({
        html: true,
        color: 'yellow-1',
        textColor: 'green',
        message:
          '<strong>Successfully Added!</strong><br />New To-do List has been Added successfully!',
        position: 'bottom-right',
        actions: [
          {
            icon: 'close',
            color: 'green',
            round: true,
            size: 'sm'
          }
        ],
        classes: 'q-pa-md'
      });
    };

    const updateTask = () => {
      const updatedTask = getTaskById(Number(props.taskId));

      updatedTask.inProcessTodos = TodoInputs.value.map((todoInput) => {
        return new Todo(
          updatedTask.id,
          todoInput.taskName,
          `${todoInput.time} ${getMeridiem(todoInput.time)}`
        );
      });

      clearTodoInputs();
      $q.notify({
        html: true,
        color: 'yellow-1',
        textColor: 'green',
        message:
          '<strong>Successfully Updated!</strong><br />New To-do List has been Updated successfully',
        position: 'bottom-right',
        actions: [
          {
            icon: 'close',
            color: 'green',
            round: true,
            size: 'sm'
          }
        ],
        classes: 'q-pa-md'
      });
    };

    const handleSubmit = () => {
      if (!props.taskId) {
        createNewTask();
      } else {
        updateTask();
      }

      router.push({ name: 'dashboard' });
    };

    const handleCancel = () => {
      router.push({ name: 'dashboard' });
      clearTodoInputs();
    };

    const handleCancelTime = (form) => {
      form.showTime = false;
      form.time = null;
    };

    const handleSubmitTime = (form) => {
      form.showTime = false;
    };

    onMounted(() => {
      if (props.taskId !== undefined && props.taskId.length !== 0) {
        const id = Number(props.taskId);

        const task = getTaskById(id);
        taskTitle.value = task.title;

        setTodoInputs([]);

        const formatTime = (time) => {
          let timeArray = time.split(':');
          if (timeArray[0].length === 1) {
            timeArray[0] = `0${timeArray[0]}`;
          }

          return timeArray.join(':').slice(0, -2);
        };

        task.inProcessTodos.forEach((inProcessTodo) => {
          addNewTodoInputs(inProcessTodo.name, formatTime(inProcessTodo.time));
        });
      }
    });

    return {
      router,
      taskTitle,
      formInputs: TodoInputs,
      handleSubmit,
      handleCancel,
      addNewTodoInputs,
      removeTodoInputsById,
      handleCancelTime,
      handleSubmitTime
    };
  }
});
